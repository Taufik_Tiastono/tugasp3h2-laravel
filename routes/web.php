<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//welcome Laravel
Route::get('/', function () {
    return view('welcome');
});

//halaman index di localhost:8000/
/* Route::get('/', function () {
    return view('index');
}); */

//route localhost:8000/hello -> menampilkan Hello Dunia
/* Route::get('/hello',function(){
    return "Hello Dunia";
}); */

//pemanggilan jika blade berada di folder resources/views/halaman
/* Route::get('/hello',function(){
    return view('halaman.index');
}); */


/* Route::get('/home', function () {
    return view('index');
}); */

//menggunakan IndexController
Route::get('/', 'IndexController@index');

/* Route::get('/register', function () {
    return view('form');
}); */

//menggunakan AuthController
Route::get('/register','AuthController@formDaftar');

/* Route::post('/welcome', function () {
    return view('selamatDatang');
}); */

//Menggunakan AuthController
Route::post('/welcome','AuthController@kirim');


//CRUD Game
//create
Route::get('/game/create' , 'GameController@create');

Route::post('/game' , 'GameController@store');


//Read
Route::get('/game', 'GameController@index');

//detail
Route::get('/game/{game_id}', 'GameController@show');

//edit
Route::get('/game/{game_id}/edit', 'GameController@edit');

Route::put('/game/{game_id}', 'GameController@update');

//delete
Route::delete('/game/{game_id}', 'GameController@destroy');
