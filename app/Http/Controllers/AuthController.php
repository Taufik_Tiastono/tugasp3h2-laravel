<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formDaftar (){
        return view('form');
    }

    public function kirim (Request $request){
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];
        return view('selamatDatang', compact('namadepan','namabelakang'));
    }
}
