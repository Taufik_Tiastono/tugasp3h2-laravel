<!DOCTYPE html>
<html>

<head>
    <title>form</title>
</head>

<body>
    <form action="/welcome" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="fname">First name :</label>
        <br>
        <input type="text" id="fname" name="fname">
        <br>
        <br>
        <label for="lname">Last name :</label>
        <br>
        <input type="text" id="lname" name="lname">
        <br>
        <br>
        <label for="gender">Gender :</label>
        <br>
        <input type="radio" id="male" name="gd" value="male">Male
        <br>
        <input type="radio" id="female" name="gd" value="female">Female
        <br>
        <input type="radio" id="other" name="gd" value="other">Other
        <br>
        <br>
        <label for="ntn">Nationality :</label>
        <br>
        <select name="nation" id="nation">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br>
        <br>
        <label>Language Spoken :</label>
        <br>
        <input type="checkbox" id="language1" name="language1" value="bahasa">Bahasa Indonesia
        <br>
        <input type="checkbox" id="language2" name="language2" value="english">English
        <br>
        <input type="checkbox" id="language3" name="language3" value="other">Other
        <br>
        <br>
        <label for="bio">Bio :</label>
        <br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
        <br>
        <br>
        <input type="submit" value="Sign Up">
    </form>

</body>

</html>